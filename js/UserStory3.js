function loadEvents() {
    document.getElementById("registrarButton").addEventListener('click', postUserJSON);
}
function alertOk() {
    alert("Se ha creado el usuario correctamente");
}
function alertError() {
    alert("Debes añadir los dos campos para crear un usuario");
}
function sendData() {
    user_id = document.getElementById("inputUsername").value;
    password = document.getElementById("inputPassword").value;

    data = JSON.stringify({ "user_id": user_id, "password": password });
    alertOk();

    return data;
}

function postUserJSON() {
    user_id = document.getElementById("inputUsername").value;
    password = document.getElementById("inputPassword").value;

    if (user_id.length != 0 || password.length != 0) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "http://localhost:5000/user", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(sendData());
    } else {
        alertError();
    }
}