let buttonContador = 0;

function loadEvents() {
    document.getElementById("ficharButton").addEventListener('click', getUserJSON);
    document.getElementById("historialButton").addEventListener('click', loadHTMLHistorial);
}
function alertOk(tipo) {
    alert("Has fichado la " + tipo + " correctamente");
}
function alertError() {
    alert("Error en el usuario o en la contraseña");
}
function getCurrentDate() {
    var currentDate = new Date();
    var dateTime = currentDate.getDate() + "/" +
        (currentDate.getMonth() + 1) + "/" +
        currentDate.getFullYear() + " " +
        currentDate.getHours() + ":" +
        currentDate.getMinutes();

    return dateTime;
}
function changeButtonColour() {
    var ficharButton = document.getElementById("ficharButton");

    if (buttonContador % 2 == 0) {
        alertOk("entrada");
        ficharButton.style.backgroundColor = "blue";
        ficharButton.textContent = "Fichar Salida";

    } else {
        alertOk("salida");
        ficharButton.style.backgroundColor = "green";
        ficharButton.textContent = "Fichar Entrada";
    }
    buttonContador++;
}
function getTipo() {
    if (buttonContador % 2 == 0) {
        return "salida";
    } else
        return "entrada";
}

function loadHTMLHistorial() {
    var user_id = document.getElementById("inputUsername").value;
    window.location.replace("UserStory2.html?user_id=" + user_id);
}
function checkPassword(password) {
    var passwordInput = document.getElementById("inputPassword").value;
    if (password === passwordInput) {
        return true;
    } else {
        return false;
    }
}
function processJSONUsuario() {
    if (this.readyState == 4 && this.status == 200) {

        var obj = JSON.parse(this.responseText);

        var password = obj[0].password;

        if (checkPassword(password)) {
            changeButtonColour();
            postJSONFichaje();
        } else {
            alertError();
        }

    }
}

function sendData() {
    var user_id = document.getElementById("inputUsername").value;

    var data = JSON.stringify({
        "user_id": user_id,
        "date": getCurrentDate(),
        "tipo": getTipo(),
        "pos": { "altitud": 15, "latitud": 15 }
    });
    return data;
}

function getUserJSON() {
    var user_id = document.getElementById("inputUsername").value;
    var password = document.getElementById("inputPassword").value;

    if (user_id.length != 0 || password.length != 0) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = processJSONUsuario;
        xhr.open("GET", "http://localhost:5000/user/" + user_id, true);
        xhr.send();
    } else {
        alertError();
    }
}

function postJSONFichaje() {
    var user_id = document.getElementById("inputUsername").value;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:5000/user/" + user_id + "/fichaje", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(sendData());
}
